/* 
 * File:   main.cpp
 * Author: Princess
 *
 * Created on March 11, 2014, 11:18 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    
      // Prompt the user for an integer number
    // Always prompt the user
    cout << "Enter an integer: " << endl;
    
    int num;
    cin >> num;
    
    // Determine if number is positive or negative
    // Binary decision - either/or
//    if (num > 0)
//    {
//        cout << "Your number is positive" << endl;
//    }
//    else // Ultimatum. Always will be executed if       
//    {    //no other condition is satisfied
//        cout << "Your number is negative" << endl;
//    }
    
    // If-else block/code
    if (num > 0)
    {
        cout << "Your number is positive" << endl;
    }
    else if (num < 0)
    {
        cout << "Your number is negative" << endl;
    }
    else
    {
        cout << "You entered the number 0" << endl;
    }
    

    return 0;
}

