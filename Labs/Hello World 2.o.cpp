/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on August 20, 2013, 2:45 PM
 */
#include <cstdlib> //cstdlib library not needed
#include <iostream> //iostream is needed for output

//gives the context of where my libraries are coming from
using namespace std;

/*
 * 
 */
// There is always and only one main
// Programs always start at main
// Programs execute from top to bottom, left to right
int main(int argc, char* argv) {

    // cout specifies output
    // endl creates a new line
    // << specifies stream operator
    // All statements end in a semicolon
    
    // Using a programmer-defined identifier/variable
    // message is a variable
    // string is a data type
    // = is an assignment operator
    // assign right to left
    // string message = "Hello World";
    string message;
    message = "Hello World"; // Variable initialization
    
    cout << message << endl;
    
    // C++ ignored white space
    cout 
    << 
            "Princess Jenkins";
    
   
    // If program hits return, it ran successfully 
    return 0;
    // All my code is within curly braces
}

