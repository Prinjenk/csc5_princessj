/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 18, 2014, 11:44 AM
 */

#include <cstdlib>
#include <iostream> // adds i/o to my program
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    cout << "Hello World" << endl; // Semicolon denotes the end of a statement
    return 0;
}

