/* 
 * File:   main.cpp
 * Author: Princess
 *
 * Created on April 7, 2014, 3:05 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    char response ;
    
    cout << "Which program would you like to use?" << endl;
    cout << "Press 'a' for the Biggest number game." << endl;
    cout << "Press 'b' for the ice cream game." << endl ;
    cout << "Press 'c' for the weight measurer." << endl;
    cout << "Press 'd' for the length measurer." << endl;
    cout << "Press 'e' for the change calculator." << endl;
    cin >> response ;
    
    if (response == 'a')
    {
        
       double num1, num2, num3, maxnum;
    
    // The purpose of this first program is to collect three numbers 
    // and output the largest of the three
    

    {
        cout << "Please enter three numbers: " << endl;
        
        cin >> num1 >> num2 >> num3 ;
        
        //These "if" and "else if" statements will determine which number 
        //is the greatest of the three
        
        if (num1 > num2 && num1 > num3 )
        {
            maxnum = num1;
         }
        
        else if (num2 > num1 && num2 > num3 )
        { maxnum = num2;
        
    }
    
        else if (num3 > num1 && num3 > num2)
        {
            maxnum = num3;
        }
        
 //I am not outputting the largest value
     
     cout << "The biggest number is... " << maxnum << endl << endl;  
        
    }      
        
    }
   
    
    else if (response == 'b') 
    {
        
         //This second program will take the amount of customers that want ice cream
    // and output the total amount per person and as a group.
    
    
    cout << "Lets play the ice cream game!" << endl ;
    
    double peoplenum, ict, icp, scoops ;
    
    //ict = ice cream total
    //icp = ice cream per person
    
    cout << "How any people are in your party? " ;
    cin >> peoplenum ;
    
    cout << "How many scoops does each person want? ";
    cin >> scoops;
    
    
    ict = peoplenum * scoops ;
            icp = scoops ;
    
            cout << "If there are " << peoplenum << " people in your party... " << endl ;
            cout << "Then there will be " << ict << " scoops total and " << icp << " scoops per person. " << endl;
            cout << "Is that correct?" << endl; 
  string confirmation;          
            cin >> confirmation ;
            
       
            while (confirmation == "no")
            {
                
             cout << "I'm sorry, please allow me to fix that!" << endl;
                
                 cout << "How any people are in your party? " ;
    cin >> peoplenum ;
    
    cout << "How many scoops does each person want? ";
    cin >> scoops;
    
    
    ict = peoplenum * scoops ;
            icp = scoops ;
    
            cout << "If there are " << peoplenum << " people in your party... " << endl ;
            cout << "Then there will be " << ict << " scoops total and " << icp << " scoops per person. " << endl;
            cout << "Is that correct?" << endl; 
             cin >> confirmation ;   
             
              if (confirmation == "yes")
            {
                cout << "I'm so glad I heard your order correctly, proceed to the next window to receive your ice cream.";
            }
                
            }
        
    }
    
    else if (response == 'c')
    {
        
        string answer;
        cout << "You would like something measured by weight? ('yes' or 'no')" ;
        while (answer == "yes")
        {
            
              // writing a program that takes the weight in pounds and ounces
    
    //and the outputs the same weight in kilos and grams 
        double  pound, kilo, gram, ounce ;
        cout << "Please enter the weight of your object in pounds and ounces.\n " ;
        cout << "First, how many pounds is the object? " ;
        cin >> pound ;
        cout << "And now, the remaining ounces, please? " ;
        cin >> ounce ;
 
        //declaring my variables
       
        kilo = 2.2046 * pound ;
        gram = ounce * 28.34952 ;
        
        cout << pound << " pound(s) and " << ounce << " ounce(s) is..." << endl;
        cout << kilo << " kilograms and " << gram << " grams." << endl;
       
        }
       if (answer == "no") 
       {
           cout << "Please come again!" << endl;
    }
    }     
       else  if (response == 'd')
    
        {
           
            string answerlength;
        cout << "You would like something measured by length? ('yes' or 'no')" ;
        cin >> answerlength;
        while (answerlength == "yes")
        {
            
              // writing a program that takes the weight in feet and inches
    //and the outputs the same weight in meters and centimeters
        double feet, inches, meter, centi ;
        cout << "Please enter the weight of your object in pounds and ounces.\n " ;
        cout << "First, how many feet is the object? " ;
        cin >> feet ;
        cout << "And now, the remaining inches, please? " ;
        cin >> inches ;
 
        //declaring my variables
       
        meter = feet * .3048 ;
        centi = inches * .3937 ;
        
        cout << feet << " feet and " << inches << " inch(es) is..." << endl;
        cout << meter << " meters and " << centi << " centimeters." << endl;
       
      
        
        
        }
       if (answerlength == "no") 
       {
           cout << "Please come again!" << endl;
    }
            
        }
    
       else if (response == 'e')
       {
           //This program will give 3 their change
           //in quarters, dimes, nickels, and pennies
           double moneyowed, moneypaid, change, changeout; 
                   int quarters, dimes, nickles, pennies;
           
           cout << "Please enter the amount of money your bill is: " ;
           cin >> moneyowed;
           cout << endl;
           cout << "Please enter the amount of money you will be paying with:" ;
           cin >> moneypaid ;
           
           change = moneypaid - moneyowed ;
           quarters = change / .25 ;
           dimes = (change - quarters) * .10 ; 
  
                  
           cout <<"Your change will be... " << endl;
           cout << quarters << " quarters" << endl;
           cout << dimes << " dimes" << endl;
       }
     
    
    
            
    return 0;
}

